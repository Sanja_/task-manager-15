package ru.karamyshev.taskmanager.constant;

public interface ArgumentConst {

    String HELP = "-h";

    String VERSION = "-v";

    String ABOUT = "-a";

    String INFO = "-i";

    String COMMANDS = "-cmd";

    String ARGUMENTS = "-arg";

    String TASK_LIST = "-tsklst";

    String TASK_CLEAR = "-tskclr";

    String TASK_CREATE = "-tskcrt";

    String PROJECT_LIST = "-prlst";

    String PROJECT_CLEAR = "-prtclr";

    String PROJECT_CREATE = "-prtcrt";

    String TASK_UPDATE_BY_INDEX = "-tskupind";

    String TASK_UPDATE_BY_ID = "-tskupid";

    String TASK_VIEW_BY_ID = "-tskvid";

    String TASK_VIEW_BY_INDEX = "-tskvind";

    String TASK_VIEW_BY_NAME = "-tskvwnm";

    String TASK_REMOVE_BY_ID = "-tskrmvid";

    String TASK_REMOVE_BY_INDEX = "-tskrmvind";

    String TASK_REMOVE_BY_NAME = "-tskrmvnm";

    String PROJECT_UPDATE_BY_INDEX = "-prtupind";

    String PROJECT_UPDATE_BY_ID = "-prtupid";

    String PROJECT_VIEW_BY_ID = "-prtvwid";

    String PROJECT_VIEW_BY_INDEX = "-prtvwind";

    String PROJECT_VIEW_BY_NAME = "-prtvwnm";

    String PROJECT_REMOVE_BY_ID = "-prtrmvid";

    String PROJECT_REMOVE_BY_INDEX = "-prtrmvind";

    String PROJECT_REMOVE_BY_NAME = "-prtrmvnm";

    String LOGIN = "-lgin";

    String LOGOUT = "-lgout";

    String REGISTRY = "-rgstr";

    String SHOW_PROFILE = "-shw-prfl";

    String RENAME_PASSWORD = "-rnm-psswrd";

    String RENAME_LOGIN = "rnm-lgn";
}
