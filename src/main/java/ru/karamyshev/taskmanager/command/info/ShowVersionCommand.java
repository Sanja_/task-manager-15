package ru.karamyshev.taskmanager.command.info;

import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.constant.ArgumentConst;
import ru.karamyshev.taskmanager.constant.MsgCommandConst;
import ru.karamyshev.taskmanager.constant.TerminalConst;

public class ShowVersionCommand extends AbstractCommand {
    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    public void execute() {
        System.out.println("\n [VERSION]");
        System.out.println("1.0.8");
    }

}
