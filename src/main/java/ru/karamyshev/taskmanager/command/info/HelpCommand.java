package ru.karamyshev.taskmanager.command.info;

import ru.karamyshev.taskmanager.api.service.ICommandService;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    public HelpCommand() {
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("\n [HELP]");
        final ICommandService commandService = IServiceLocator.getCommandService();
        final Collection<AbstractCommand> commands = commandService.getTerminalCommands();

        for (final AbstractCommand command : commands){
            System.out.println(command.name() + " (" +command.arg() + ") : -" + command.description());

        }
        System.out.println("[OK]");
    }
}
