package ru.karamyshev.taskmanager.command.user;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class RenameUserPasswordCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-rnm-psswrd";
    }

    @Override
    public String name() {
        return "rename-password";
    }

    @Override
    public String description() {
        return "Rename password account.";
    }

    @Override
    public void execute() {
        System.out.println("CHANGE ACCOUNT PASSWORD");
        IAuthService authService = IServiceLocator.getAuthService();
        String userId = authService.getUserId();
        System.out.println("[ENTER OLD PASSWORD]");
        String oldPassword = TerminalUtil.nextLine();
        System.out.println("[ENTER NEW PASSWORD]");
        String newPassword = TerminalUtil.nextLine();
        String currentLogin = authService.getCurrentLogin();
        authService.renamePassword(userId, currentLogin , oldPassword, newPassword);
        System.out.println("[OK]");
    }

}
