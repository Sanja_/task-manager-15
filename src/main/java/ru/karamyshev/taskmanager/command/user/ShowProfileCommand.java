package ru.karamyshev.taskmanager.command.user;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.entity.User;

public class ShowProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-shw-prfl";
    }

    @Override
    public String name() {
        return "show-profile";
    }

    @Override
    public String description() {
        return "Show profile.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROFILE]");
        IAuthService authService = IServiceLocator.getAuthService();
        String currentLogin = authService.getCurrentLogin();
        String userId = authService.getUserId();
        User user = authService.showProfile(userId, currentLogin);
        System.out.println("LOGIN: " +user.getLogin());
        System.out.println("HASH PASSWORD: " +user.getPasswordHash());
        System.out.println("[OK]");
    }
}
