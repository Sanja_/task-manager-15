package ru.karamyshev.taskmanager.command.project;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.command.AbstractCommand;

public class ClearProjectCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-prtclr";
    }

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        IAuthService authService = IServiceLocator.getAuthService();
        IProjectService projectService = IServiceLocator.getProjectService();
        final String userId = authService.getUserId();
        projectService.clear(userId);
        System.out.println("[OK]");
    }
}
