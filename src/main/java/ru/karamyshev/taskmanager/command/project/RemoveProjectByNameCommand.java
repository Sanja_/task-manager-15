package ru.karamyshev.taskmanager.command.project;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.util.List;

public class RemoveProjectByNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-prtrmvnm";
    }

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT NAME FOR DELETION:");
        final IAuthService authService = IServiceLocator.getAuthService();
        final IProjectService projectService = IServiceLocator.getProjectService();
        final String userId = authService.getUserId();
        final String name = TerminalUtil.nextLine();
        final List<Project> project = projectService.removeOneByName(userId, name);
        if (project == null || project.isEmpty()) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }
}
