package ru.karamyshev.taskmanager.command.project;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UpdateProjectByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-prtupid";
    }

    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        final IAuthService authService = IServiceLocator.getAuthService();
        final IProjectService projectService = IServiceLocator.getProjectService();
        final String userId = authService.getUserId();
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectById(userId, id,name,description);
        if (projectUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }
}
