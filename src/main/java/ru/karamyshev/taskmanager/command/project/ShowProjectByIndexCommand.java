package ru.karamyshev.taskmanager.command.project;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class ShowProjectByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-prtvwind";
    }

    @Override
    public String name() {
        return "project-view-by-index";
    }

    @Override
    public String description() {
        return "Show project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final IAuthService authService = IServiceLocator.getAuthService();
        final IProjectService projectService = IServiceLocator.getProjectService();
        final String userId = authService.getUserId();
        final Integer index = TerminalUtil.nextNumber();
        final Project project = projectService.findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProjects(project);
        System.out.println("[OK]");
    }

    private void showProjects(final Project project) {
        if (project == null) return;
        System.out.println("ID:" + project.getId());
        System.out.println("NAME:" + project.getName());
        System.out.println("DESCRIPTION:" + project.getDescription());
    }
}
