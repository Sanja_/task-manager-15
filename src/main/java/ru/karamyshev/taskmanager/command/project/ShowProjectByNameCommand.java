package ru.karamyshev.taskmanager.command.project;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.util.List;

public class ShowProjectByNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-prtvwnm";
    }

    @Override
    public String name() {
        return "project-view-by-name";
    }

    @Override
    public String description() {
        return "Show project by name.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final IAuthService authService = IServiceLocator.getAuthService();
        final IProjectService projectService = IServiceLocator.getProjectService();
        final String userId = authService.getUserId();
        final String name = TerminalUtil.nextLine();
        final List<Project> project = projectService.findOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        for (Project proj: project){
            showProjects(proj);
        }
        System.out.println("[OK]");
    }

    private void showProjects(final Project project) {
        if (project == null) return;
        System.out.println("ID:" + project.getId());
        System.out.println("NAME:" + project.getName());
        System.out.println("DESCRIPTION:" + project.getDescription());
    }
}
