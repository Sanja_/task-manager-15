package ru.karamyshev.taskmanager.command.project;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UpdateProjectByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-prtupind";
    }

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        final IAuthService authService = IServiceLocator.getAuthService();
        final IProjectService projectService = IServiceLocator.getProjectService();
        final String userId = authService.getUserId();
        final Integer index = TerminalUtil.nextNumber();
        final Project Project = projectService.findOneByIndex(userId, index);
        if (Project == null) {
            System.out.println("[FAIL]");
            return ;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectByIndex(userId, index, name, description);
        if (projectUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }
}
