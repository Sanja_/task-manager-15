package ru.karamyshev.taskmanager.command;

import ru.karamyshev.taskmanager.api.service.IServiceLocator;

public abstract class AbstractCommand {

  protected IServiceLocator IServiceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator IServiceLocator) {
        this.IServiceLocator = IServiceLocator;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute();

}
