package ru.karamyshev.taskmanager.command.system;

import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class RegistryUserCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-rgstr";
    }

    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String description() {
        return "Registration new account.";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL");
        final String email = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = TerminalUtil.nextLine();
        IServiceLocator.getAuthService().registry(login, password, email);
        System.out.println("[OK]");
    }

}
