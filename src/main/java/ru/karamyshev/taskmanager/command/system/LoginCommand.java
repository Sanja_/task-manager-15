package ru.karamyshev.taskmanager.command.system;

import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class LoginCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-lgin";
    }

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String description() {
        return "Login in account.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = TerminalUtil.nextLine();
        IServiceLocator.getAuthService().login(login, password);
        System.out.println("[OK]");
    }

}
