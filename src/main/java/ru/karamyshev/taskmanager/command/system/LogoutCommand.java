package ru.karamyshev.taskmanager.command.system;

import ru.karamyshev.taskmanager.command.AbstractCommand;

public class LogoutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-lgout";
    }

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout in account.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        IServiceLocator.getAuthService().logout();
        System.out.println("[OK]");
    }

}
