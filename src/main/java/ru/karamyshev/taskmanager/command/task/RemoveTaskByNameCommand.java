package ru.karamyshev.taskmanager.command.task;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.util.List;

public class RemoveTaskByNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-tskrmvnm";
    }

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK NAME FOR DELETION:");
        IAuthService authService = IServiceLocator.getAuthService();
        ITaskService taskService = IServiceLocator.getTaskService();
        final String userId = authService.getUserId();
        final String name = TerminalUtil.nextLine();
        final List<Task> task = taskService.removeOneByName(userId, name);
        if (task == null || task.isEmpty()) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
