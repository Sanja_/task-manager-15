package ru.karamyshev.taskmanager.command.task;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class ShowTaskByIdCommand extends AbstractCommand {
    @Override
    public String arg() {
        return "-tskvid";
    }

    @Override
    public String name() {
        return "task-view-by-id";
    }

    @Override
    public String description() {
        return "Show task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        IAuthService authService = IServiceLocator.getAuthService();
        ITaskService taskService = IServiceLocator.getTaskService();
        final String userId = authService.getUserId();
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("\n");
        System.out.println("ID:" + task.getId());
        System.out.println("NAME:" + task.getName());
        System.out.println("DESCRIPTION:" + task.getDescription());
    }

}
