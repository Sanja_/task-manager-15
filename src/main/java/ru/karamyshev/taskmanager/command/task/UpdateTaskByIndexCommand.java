package ru.karamyshev.taskmanager.command.task;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UpdateTaskByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-tskupind";
    }

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        IAuthService authService = IServiceLocator.getAuthService();
        ITaskService taskService = IServiceLocator.getTaskService();
        final String userId = authService.getUserId();
        final Integer index = TerminalUtil.nextNumber();
        final Task task = taskService.findOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return ;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = taskService.updateTaskByIndex(userId, index, name, description);
        if (taskUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }
}
