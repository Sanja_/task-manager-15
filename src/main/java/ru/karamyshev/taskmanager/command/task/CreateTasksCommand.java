package ru.karamyshev.taskmanager.command.task;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class CreateTasksCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-tskcrt";
    }

    @Override
    public String name() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK");
        System.out.println("ENTER NAME:");
        IAuthService authService = IServiceLocator.getAuthService();
        ITaskService taskService = IServiceLocator.getTaskService();
        final String userId = authService.getUserId();
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        taskService.create(userId, name, description);
        System.out.println("[OK]");
    }

}
