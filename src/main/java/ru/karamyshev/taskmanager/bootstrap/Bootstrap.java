package ru.karamyshev.taskmanager.bootstrap;

import ru.karamyshev.taskmanager.api.repository.ICommandRepository;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.api.repository.IUserRepository;
import ru.karamyshev.taskmanager.api.service.*;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.constant.MsgCommandConst;
import ru.karamyshev.taskmanager.exception.CommandIncorrectException;
import ru.karamyshev.taskmanager.repository.CommandRepository;
import ru.karamyshev.taskmanager.repository.ProjectRepository;
import ru.karamyshev.taskmanager.repository.TaskRepository;
import ru.karamyshev.taskmanager.repository.UserRepository;
import ru.karamyshev.taskmanager.role.Role;
import ru.karamyshev.taskmanager.service.*;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class Bootstrap implements IServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        init(commandService.getTerminalCommands());
    }

    private void init(ArrayList<AbstractCommand> commands){
        for (AbstractCommand command: commands) registry(command);
    }

    private void registry(final AbstractCommand command){
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void initUser(){
        userService.create("test", "test", "test@test.com");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parsArgs(args);
        initUser();
        inputCommand();
    }

    private void inputCommand() {
        while (true) {
            try {
                parsCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void parsCommand(final String args) {
        validateArgs(args);
        String[] command = args.trim().split("\\s+");
        for (String arg : command) chooseResponsCommand(arg);
    }

    private void parsArgs(final String... args) {
        validateArgs(args);
        try {
            for (String arg : args) chooseResponsArg(arg.trim());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }

    }

    private void validateArgs(final String... args) {
        if (args != null || args.length > 0) return;
        System.out.println(MsgCommandConst.COMMAND_ABSENT);
    }

    private void chooseResponsArg(final String arg) {
        final AbstractCommand argument =  arguments.get(arg);
        if (argument == null) throw new CommandIncorrectException(arg);
        argument.execute();
    }

    private void chooseResponsCommand(final String cmd) {
       final AbstractCommand command =  commands.get(cmd);
       if (command == null) throw new CommandIncorrectException(cmd);
       command.execute();
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

}
