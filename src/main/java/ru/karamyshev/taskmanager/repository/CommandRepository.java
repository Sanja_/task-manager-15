package ru.karamyshev.taskmanager.repository;

import ru.karamyshev.taskmanager.api.repository.ICommandRepository;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.command.info.*;
import ru.karamyshev.taskmanager.command.project.*;
import ru.karamyshev.taskmanager.command.system.ExitCommand;
import ru.karamyshev.taskmanager.command.system.LoginCommand;
import ru.karamyshev.taskmanager.command.system.LogoutCommand;
import ru.karamyshev.taskmanager.command.system.RegistryUserCommand;
import ru.karamyshev.taskmanager.command.task.*;
import ru.karamyshev.taskmanager.command.user.RenameLoginCommand;
import ru.karamyshev.taskmanager.command.user.RenameUserPasswordCommand;
import ru.karamyshev.taskmanager.command.user.ShowProfileCommand;

import java.util.ArrayList;

public class CommandRepository implements ICommandRepository {

    final private ArrayList<AbstractCommand> commands = new ArrayList<>();
    {
        commands.add(new HelpCommand());
        commands.add(new SystemInfoCommand());
        commands.add(new ShowVersionCommand());
        commands.add(new AboutCommand());
        commands.add(new ShowCommandsCommand());
        commands.add(new ShowArgumentsCommand());

        commands.add(new RegistryUserCommand());
        commands.add(new ExitCommand());
        commands.add(new LogoutCommand());
        commands.add(new LoginCommand());

        commands.add(new RenameLoginCommand());
        commands.add(new RenameUserPasswordCommand());
        commands.add(new ShowProfileCommand());

        commands.add(new ShowTasksCommand());
        commands.add(new ClearTaskCommand());
        commands.add(new CreateTasksCommand());
        commands.add(new ShowTaskByIdCommand());
        commands.add(new ShowTaskByNameCommand());
        commands.add(new UpdateTaskByIdCommand());
        commands.add(new UpdateTaskByIndexCommand());
        commands.add(new RemoveTaskByIdCommand());
        commands.add(new RemoveTaskByIndexCommand());
        commands.add(new RemoveTaskByNameCommand());
        commands.add(new ShowTaskByIndexCommand());

        commands.add(new ShowProjectCommand());
        commands.add(new ClearProjectCommand());
        commands.add(new CreateProjectCommand());
        commands.add(new ShowProjectByIdCommand());
        commands.add(new ShowProjectByNameCommand());
        commands.add(new UpdateProjectByIdCommand());
        commands.add(new UpdateProjectByIndexCommand());
        commands.add(new RemoveProjectByIdCommand());
        commands.add(new RemoveProjectByIndexCommand());
        commands.add(new RemoveProjectByNameCommand());
        commands.add(new ShowProjectByIndexCommand());

    }

    private final ArrayList<String> COMMANDS = getCommands(commands);

    private final ArrayList<String> ARGS = getArgs(commands);

    public ArrayList<String> getCommands(final ArrayList<AbstractCommand> values) {
        if (values == null || values.size() == 0) return null;
        final ArrayList<String> commandsList = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            final String command = values.get(i).name();
            if (command == null || command.isEmpty()) continue;
            commandsList.add(command);
        }
        return commandsList;
    }

    public ArrayList<String> getArgs(final ArrayList<AbstractCommand> values) {
        if (values == null || values.size() == 0) return null;
        final ArrayList<String> argsList = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            final String arg = values.get(i).arg();
            if (arg == null || arg.isEmpty()) continue;
            argsList.add(arg);
        }
        return argsList;
    }

    public ArrayList<AbstractCommand> getTerminalCommands() {
        return commands;
    }

    public ArrayList<String> getCommands() {
        return COMMANDS;
    }

    public ArrayList<String> getArgs() {
        return ARGS;
    }
}
