package ru.karamyshev.taskmanager.service;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.exception.MatchLoginsException;
import ru.karamyshev.taskmanager.exception.NotMatchPasswordsException;
import ru.karamyshev.taskmanager.exception.empty.EmptyLoginException;
import ru.karamyshev.taskmanager.exception.empty.EmptyPasswordException;
import ru.karamyshev.taskmanager.exception.empty.EmptyUserIdException;
import ru.karamyshev.taskmanager.exception.user.AccessDeniedException;
import ru.karamyshev.taskmanager.util.HashUtil;

public class AuthService implements IAuthService {

    private IUserService userService;

    private String userId;

    private String currentLogin;

    public AuthService(final IUserService userService){
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public String getCurrentLogin() {
        if (currentLogin == null) throw new AccessDeniedException();
        return currentLogin;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final  String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        String userUpdateId = Long.toString(user.getId());
        currentLogin = user.getLogin();
        userId = userUpdateId;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

    @Override
    public void renamePassword(String userId, String currentLogin, String oldPassword, String newPassword) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new EmptyPasswordException();
        if (currentLogin == null || currentLogin.isEmpty()) throw new EmptyLoginException();
        final User user = userService.findByLogin(currentLogin);
        if (user == null) throw new AccessDeniedException();
        String hash = HashUtil.salt(oldPassword);
        if (!hash.equals(user.getPasswordHash())) throw new NotMatchPasswordsException();
        String newHash = HashUtil.salt(newPassword);
        user.setPasswordHash(newHash);
    }

    @Override
    public void renameLogin(String currentUserId, String currentLogin, String newLogin) {
        if (currentUserId == null || currentUserId.isEmpty()) throw new EmptyUserIdException();
        if (currentLogin == null || currentLogin.isEmpty()) throw new EmptyLoginException();
        if (newLogin == null || newLogin.isEmpty()) throw new EmptyLoginException();
        final User user = userService.findByLogin(currentLogin);
        if (user == null) throw new AccessDeniedException();
        final User newLog = userService.findByLogin(newLogin);
        if (newLog != null) throw new MatchLoginsException();
        user.setLogin(newLogin);
        String userUpdateId = Long.toString(user.getId());
        userId = userUpdateId;
        this.currentLogin = user.getLogin();
    }

    @Override
    public User showProfile(String userId, String login){
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        return user;
    }
}
