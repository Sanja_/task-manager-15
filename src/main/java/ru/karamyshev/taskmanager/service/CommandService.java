package ru.karamyshev.taskmanager.service;

import ru.karamyshev.taskmanager.api.repository.ICommandRepository;
import ru.karamyshev.taskmanager.api.service.ICommandService;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.ArrayList;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public ArrayList<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
