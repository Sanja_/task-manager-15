package ru.karamyshev.taskmanager.api.repository;

import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.ArrayList;

public interface ICommandRepository {

    ArrayList<AbstractCommand> getTerminalCommands();

}
