package ru.karamyshev.taskmanager.api.service;

import ru.karamyshev.taskmanager.entity.Project;

import java.util.List;

public interface IProjectService {
    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findOneByIndex(String userId, Integer index);

    List<Project> findOneByName(String userId, String name);

    Project updateProjectById(String userId, String id, String name, String description);

    Project removeOneByIndex(String userId, Integer index);

    List<Project> removeOneByName(String userId, String name);

    Project findOneById(String userId, String id);

    Project removeOneById(String userId, String id);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);
}
