package ru.karamyshev.taskmanager.api.service;

import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.ArrayList;

public interface ICommandService {

    ArrayList<AbstractCommand> getTerminalCommands();

}
