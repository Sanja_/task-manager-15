package ru.karamyshev.taskmanager.api.service;

public interface IServiceLocator {

     IUserService getUserService();

     IAuthService getAuthService();

     ICommandService getCommandService();

     ITaskService getTaskService();

     IProjectService getProjectService();
}
